const express = require("express");
const mongoose = require("mongoose");


//Allows our backend application to be available to our frontend application
//cross-origin resource sharing
const cors = require("cors");

//allows access to routes define within our application
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

//MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.cwrxp6r.mongodb.net/b244_booking?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once(`open`, () => 
	console.log(`Now connected to the MongoDB atlas.`));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Defines "/users" to be included for all the user routes defined in the userRoutes file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Will use the defined port number for the application whenever an environment variable is available OR will use the port 4000 if non is defined
//this syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online at port ${process.env.PORT || 4000}`)
});