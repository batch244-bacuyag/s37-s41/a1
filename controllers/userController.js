const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Course = require("../models/Course");


//CHeck if the email already exists
/*
	steps:
	1. Use "mongoose" "find method" to find duplicate emails
	2. .then method to send a response back to the frontend application based on the result of the find method
*/

module.exports.checkEmailExists = (reqbody) => {

	return User.find({email : reqbody.email}).then(result =>{

		//find method returns a record if a match is found
		if (result.length > 0){
			return true;

		//no duplicate email found	
		//the user is not yet registed in the databas	
		}else{
			return false;
		}
	})
}

//user registration
/*
	steps:
	1. create a new user object using the mongoose model and the infomation from the request body
	2. make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (reqbody) => {

	//creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	//uses the infomation from the request body to provide all the necessary information
	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		mobileNo: reqbody.mobileNo,

		//Syntax: bcrpt.hashSync(dataToBeEncrypted, salt)
		//10 times that the password will be encrypted
		password: bcrypt.hashSync(reqbody.password, 10)
	})

	//saves the create object to our database
	return newUser.save().then((user, error) =>{

		//user registration is failed
		if(error) {
			return false;

		//user registration is succesful
		}else{
			return true;
		}
	})
}

// User authentication
		/*
			Steps:
			1. Check the database if the user email exists
			2. Compare the password provided in the login form with the password stored in the database
			3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqbody) => {

	return User.findOne({email: reqbody.email}).then(result =>{
		if(result == null){
			return false
		}else{

			//Creates a variable to return the result of comparing the login from password and the database password
			//"compareSync" is used to compare a non encrypted password from the login form to the encrypted password from the database and returns "true" or "false"
			const isPasswordCorrect = bcrypt.compareSync(reqbody.password, result.password)


			//generate an access token
			//it sues the "createAccessToken" method that we defined in the auth.js
			//returnung an object back to the frontend application  is common practice to ensure information is properly labeled
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}else{
				return {access: false}
			}
		}
	})
}

// Retrieve user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend
*/
module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

//Enroll user to a course
/*Steps:
			1. Find the document in the database using the user's ID
			2. Add the course ID to the user's enrollment array
			3. Update the document in the MongoDB Atlas Database
		*/

/*module.exports.enroll = async (data) => {
    if(data.isAdmin) throw { message: 'Admins are not allowed to enroll in courses' };

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});

		return user.save().then((user, error) =>{
			if(error){
				return false;
			} else{
				return true;
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course =>{course.enrollees.push({user : data.userId})

		return course.save().then((course, error) =>{
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	});

	if (isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	}
}*/

module.exports.enroll = async (data) => {
    try {
        const course = await Course.findById(data.courseId);

        const user = await User.findById(data.userId);

        if(course && user){
        	user.enrollments.push({courseId : data.courseId});
        	course.enrollees.push({userId : data.userId});

        	user.save();
        	course.save();
        	return true;
        }

    } catch (err) {
        throw err;
    }
};

//shorter code
/*module.exports.enroll = async (data) => {
    try {
        const { userId, courseId } = data;

        const user = await User.findById(userId);
        user.enrollments.push({ courseId });
        await user.save();
        
        const course = await Course.findById(courseId);
        course.enrollees.push({ userId });
        await course.save();
        
        return true;
    } catch (error) {
        return false;
    }
}*/

